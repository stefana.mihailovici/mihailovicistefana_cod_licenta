from PyQt5 import QtCore, QtGui, QtWidgets
from det import Ui_SecondWindow
import xml.etree.ElementTree as ET
import subprocess
import time
import tkinter as tk


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1139, 764)
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        MainWindow.setFont(font)

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.car_pic = QtWidgets.QLabel(self.centralwidget)
        self.car_pic.setGeometry(QtCore.QRect(100, 60, 531, 261))
        self.car_pic.setPixmap(QtGui.QPixmap("../../../Users/Patricia/OneDrive/Desktop/car_schematic.png"))
        self.car_pic.setObjectName("car_pic")


        # Initializare stareled
        self.is_led_zat1_on = False
        self.is_led_zat2_on = False
        self.is_led_v_hr_0_on = False
        self.is_led_v_hr_1_on = False
        self.is_led_v_hl_0_on = False
        self.is_led_v_hl_1_on = False
        self.is_led_d_hr_0_on = False
        self.is_led_d_hr_1_on = False
        self.is_led_d_hl_0_on = False
        self.is_led_d_hl_1_on = False
        self.is_led_soft_0_on = False
        self.is_led_soft_1_on = False
        self.is_led_mot1_on = False

        self.is_kl15_sig_on = False
        self.is_kl15_rel_on = False
        self.is_br_l_on = False
        self.is_br_r_on = False
        self.is_sl_hl_on = False
        self.is_nsl_r_on = False
        self.is_nsl_l_on = False


        self.lbl_send_zat1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_zat1.setGeometry(QtCore.QRect(820, 40, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_zat1.setFont(font)
        self.lbl_send_zat1.setObjectName("lbl_send_zat1")

        self.lbl_send_zat2 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_zat2.setGeometry(QtCore.QRect(970, 40, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_zat2.setFont(font)
        self.lbl_send_zat2.setObjectName("lbl_send_zat2")

        self.btn_send_zat1 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_zat1.setGeometry(QtCore.QRect(810, 60, 110, 28))
        self.btn_send_zat1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_zat1.setFont(font)
        self.btn_send_zat1.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_zat1.setObjectName("btn_send_zat1")


        self.btn_send_zat1.clicked.connect(self.clicked_zat1)



        self.btn_send_zat2 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_zat2.setGeometry(QtCore.QRect(960, 60, 110, 28))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_zat2.setFont(font)
        self.btn_send_zat2.setObjectName("btn_send_zat2")


        self.btn_send_zat2.clicked.connect(self.clicked_zat2)




        self.lbl_text_zat1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_zat1.setGeometry(QtCore.QRect(190, 310, 61, 21))
        self.lbl_text_zat1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_zat1.setFont(font)
        self.lbl_text_zat1.setObjectName("lbl_text_zat1")

        self.lbl_text_zat2 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_zat2.setGeometry(QtCore.QRect(190, 330, 61, 21))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_zat2.setFont(font)
        self.lbl_text_zat2.setObjectName("lbl_text_zat2")

        self.lbl_led_zat1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_zat1.setGeometry(QtCore.QRect(170, 310, 16, 16))
        self.lbl_led_zat1.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_zat1.setToolTipDuration(-5)
        self.lbl_led_zat1.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_zat1.setText("")
        self.lbl_led_zat1.setObjectName("lbl_led_zat1")
        self.lbl_led_zat2 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_zat2.setGeometry(QtCore.QRect(170, 330, 16, 16))
        self.lbl_led_zat2.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_zat2.setToolTipDuration(-5)
        self.lbl_led_zat2.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_zat2.setText("")
        self.lbl_led_zat2.setObjectName("lbl_led_zat2")

        self.btn_Details = QtWidgets.QPushButton(self.centralwidget, clicked = lambda: self.openWindow())
        self.btn_Details.setGeometry(QtCore.QRect(920, 590, 191, 61))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(12)
        self.btn_Details.setFont(font)
        self.btn_Details.setObjectName("btn_Details")


        self.btn_Refresh = QtWidgets.QPushButton(self.centralwidget)
        self.btn_Refresh.setGeometry(QtCore.QRect(600, 580, 81, 61))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(12)
        self.btn_Refresh.setFont(font)
        self.btn_Refresh.setObjectName("btn_Refresh")

        self.btn_Refresh.clicked.connect(self.refresh)


        self.lbl_send_v_hr_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_v_hr_0.setGeometry(QtCore.QRect(820, 100, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_v_hr_0.setFont(font)
        self.lbl_send_v_hr_0.setObjectName("lbl_send_v_hr_0")

        self.lbl_send_v_hr_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_v_hr_1.setGeometry(QtCore.QRect(970, 100, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_v_hr_1.setFont(font)
        self.lbl_send_v_hr_1.setObjectName("lbl_send_v_hr_1")

        self.btn_send_v_hr_0 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_v_hr_0.setGeometry(QtCore.QRect(810, 120, 110, 28))
        self.btn_send_v_hr_0.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_v_hr_0.setFont(font)
        self.btn_send_v_hr_0.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_v_hr_0.setObjectName("btn_send_v_hr_0")


        self.btn_send_v_hr_0.clicked.connect(self.clicked_vhr_0)




        self.btn_send_v_hr_1 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_v_hr_1.setGeometry(QtCore.QRect(960, 120, 110, 28))
        self.btn_send_v_hr_1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_v_hr_1.setFont(font)
        self.btn_send_v_hr_1.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_v_hr_1.setObjectName("btn_send_v_hr_1")


        self.btn_send_v_hr_1.clicked.connect(self.clicked_vhr_1)





        self.lbl_send_v_hl_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_v_hl_0.setGeometry(QtCore.QRect(820, 160, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_v_hl_0.setFont(font)
        self.lbl_send_v_hl_0.setObjectName("lbl_send_v_hl_0")

        self.lbl_send_v_hl_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_v_hl_1.setGeometry(QtCore.QRect(970, 160, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_v_hl_1.setFont(font)
        self.lbl_send_v_hl_1.setObjectName("lbl_send_v_hl_1")

        self.btn_send_v_hl_0 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_v_hl_0.setGeometry(QtCore.QRect(810, 180, 110, 28))
        self.btn_send_v_hl_0.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_v_hl_0.setFont(font)
        self.btn_send_v_hl_0.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_v_hl_0.setObjectName("btn_send_v_hl_0")


        self.btn_send_v_hl_0.clicked.connect(self.clicked_vhl_0)





        self.btn_send_v_hl_1 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_v_hl_1.setGeometry(QtCore.QRect(960, 180, 110, 28))
        self.btn_send_v_hl_1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_v_hl_1.setFont(font)
        self.btn_send_v_hl_1.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_v_hl_1.setObjectName("btn_send_v_hl_1")


        self.btn_send_v_hl_1.clicked.connect(self.clicked_vhl_1)




        self.lbl_send_d_hr_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_d_hr_0.setGeometry(QtCore.QRect(820, 220, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_d_hr_0.setFont(font)
        self.lbl_send_d_hr_0.setObjectName("lbl_send_d_hr_0")


        self.btn_send_d_hr_0 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_d_hr_0.setGeometry(QtCore.QRect(810, 240, 110, 28))
        self.btn_send_d_hr_0.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_d_hr_0.setFont(font)
        self.btn_send_d_hr_0.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_d_hr_0.setObjectName("btn_send_d_hr_0")


        self.btn_send_d_hr_0.clicked.connect(self.clicked_dhr_0)




        self.lbl_send_d_hr_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_d_hr_1.setGeometry(QtCore.QRect(970, 220, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_d_hr_1.setFont(font)
        self.lbl_send_d_hr_1.setObjectName("lbl_send_d_hr_1")

        self.btn_send_d_hr_1 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_d_hr_1.setGeometry(QtCore.QRect(960, 240, 110, 28))
        self.btn_send_d_hr_1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_d_hr_1.setFont(font)
        self.btn_send_d_hr_1.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_d_hr_1.setObjectName("btn_send_d_hr_1")


        self.btn_send_d_hr_1.clicked.connect(self.clicked_dhr_1)








        self.lbl_send_d_hl_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_d_hl_0.setGeometry(QtCore.QRect(820, 280, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_d_hl_0.setFont(font)
        self.lbl_send_d_hl_0.setObjectName("lbl_send_d_hl_0")

        self.lbl_send_d_hl_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_d_hl_1.setGeometry(QtCore.QRect(970, 280, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_d_hl_1.setFont(font)
        self.lbl_send_d_hl_1.setObjectName("lbl_send_d_hl_1")

        self.btn_send_d_hl_0 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_d_hl_0.setGeometry(QtCore.QRect(810, 300, 110, 28))
        self.btn_send_d_hl_0.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_d_hl_0.setFont(font)
        self.btn_send_d_hl_0.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_d_hl_0.setObjectName("btn_send_d_hl_0")


        self.btn_send_d_hl_0.clicked.connect(self.clicked_dhl_0)





        self.btn_send_d_hl_1 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_d_hl_1.setGeometry(QtCore.QRect(960, 300, 110, 28))
        self.btn_send_d_hl_1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_d_hl_1.setFont(font)
        self.btn_send_d_hl_1.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_d_hl_1.setObjectName("btn_send_d_hl_1")



        self.btn_send_d_hl_1.clicked.connect(self.clicked_dhl_1)




        self.lbl_send_soft_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_soft_0.setGeometry(QtCore.QRect(820, 340, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_soft_0.setFont(font)
        self.lbl_send_soft_0.setObjectName("lbl_send_soft_0")

        self.lbl_send_soft_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_soft_1.setGeometry(QtCore.QRect(970, 340, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_soft_1.setFont(font)
        self.lbl_send_soft_1.setObjectName("lbl_send_soft_1")

        self.btn_send_soft_0 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_soft_0.setGeometry(QtCore.QRect(810, 360, 110, 28))
        self.btn_send_soft_0.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_soft_0.setFont(font)
        self.btn_send_soft_0.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_soft_0.setObjectName("btn_send_soft_0")


        self.btn_send_soft_0.clicked.connect(self.clicked_soft_0)




        self.btn_send_soft_1 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_soft_1.setGeometry(QtCore.QRect(960, 360, 110, 28))
        self.btn_send_soft_1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_soft_1.setFont(font)
        self.btn_send_soft_1.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_soft_1.setObjectName("btn_send_soft_1")


        self.btn_send_soft_1.clicked.connect(self.clicked_soft_1)





        self.lbl_send_mot1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_send_mot1.setGeometry(QtCore.QRect(820, 400, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_send_mot1.setFont(font)
        self.lbl_send_mot1.setObjectName("lbl_send_mot1")

        self.btn_send_mot1 = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_mot1.setGeometry(QtCore.QRect(810, 420, 110, 28))
        self.btn_send_mot1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.btn_send_mot1.setFont(font)
        self.btn_send_mot1.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_send_mot1.setObjectName("btn_send_mot1")



        self.btn_send_mot1.clicked.connect(self.clicked_mot1)











        self.lbl_led_mot1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_mot1.setGeometry(QtCore.QRect(30, 180, 16, 16))
        self.lbl_led_mot1.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_mot1.setToolTipDuration(-5)
        self.lbl_led_mot1.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_mot1.setText("")
        self.lbl_led_mot1.setObjectName("lbl_led_mot1")
        self.lbl_text_mot1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_mot1.setGeometry(QtCore.QRect(10, 200, 91, 21))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_mot1.setFont(font)
        self.lbl_text_mot1.setObjectName("lbl_text_mot1")
        self.lbl_led_soft_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_soft_0.setGeometry(QtCore.QRect(610, 150, 16, 16))
        self.lbl_led_soft_0.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_soft_0.setToolTipDuration(-5)
        self.lbl_led_soft_0.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_soft_0.setText("")
        self.lbl_led_soft_0.setObjectName("lbl_led_soft_0")
        self.lbl_led_soft_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_soft_1.setGeometry(QtCore.QRect(610, 210, 16, 16))
        self.lbl_led_soft_1.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_soft_1.setToolTipDuration(-5)
        self.lbl_led_soft_1.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_soft_1.setText("")
        self.lbl_led_soft_1.setObjectName("lbl_led_soft_1")
        self.lbl_text_soft_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_soft_0.setGeometry(QtCore.QRect(630, 150, 101, 21))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_soft_0.setFont(font)
        self.lbl_text_soft_0.setObjectName("lbl_text_soft_0")
        self.lbl_text_soft_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_soft_1.setGeometry(QtCore.QRect(630, 210, 101, 21))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_soft_1.setFont(font)
        self.lbl_text_soft_1.setObjectName("lbl_text_soft_1")
        self.lbl_led_v_hr_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_v_hr_0.setGeometry(QtCore.QRect(390, 30, 16, 16))
        self.lbl_led_v_hr_0.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_v_hr_0.setToolTipDuration(-5)
        self.lbl_led_v_hr_0.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_v_hr_0.setText("")
        self.lbl_led_v_hr_0.setObjectName("lbl_led_v_hr_0")
        self.lbl_led_v_hr_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_v_hr_1.setGeometry(QtCore.QRect(390, 50, 16, 16))
        self.lbl_led_v_hr_1.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_v_hr_1.setToolTipDuration(-5)
        self.lbl_led_v_hr_1.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_v_hr_1.setText("")
        self.lbl_led_v_hr_1.setObjectName("lbl_led_v_hr_1")
        self.lbl_text_v_hr_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_v_hr_0.setGeometry(QtCore.QRect(410, 30, 55, 21))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_v_hr_0.setFont(font)
        self.lbl_text_v_hr_0.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl_text_v_hr_0.setObjectName("lbl_text_v_hr_0")
        self.lbl_text_v_hr_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_v_hr_1.setGeometry(QtCore.QRect(410, 50, 55, 21))
        self.lbl_text_v_hr_1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_v_hr_1.setFont(font)
        self.lbl_text_v_hr_1.setObjectName("lbl_text_v_hr_1")
        self.lbl_text_v_hl_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_v_hl_0.setGeometry(QtCore.QRect(300, 30, 55, 21))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_v_hl_0.setFont(font)
        self.lbl_text_v_hl_0.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl_text_v_hl_0.setObjectName("lbl_text_v_hl_0")
        self.lbl_led_v_hl_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_v_hl_0.setGeometry(QtCore.QRect(280, 30, 16, 16))
        self.lbl_led_v_hl_0.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_v_hl_0.setToolTipDuration(-5)
        self.lbl_led_v_hl_0.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_v_hl_0.setText("")
        self.lbl_led_v_hl_0.setObjectName("lbl_led_v_hl_0")
        self.lbl_text_v_hl_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_v_hl_1.setGeometry(QtCore.QRect(300, 50, 55, 21))
        self.lbl_text_v_hl_1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_v_hl_1.setFont(font)
        self.lbl_text_v_hl_1.setObjectName("lbl_text_v_hl_1")
        self.lbl_led_v_hl_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_v_hl_1.setGeometry(QtCore.QRect(280, 50, 16, 16))
        self.lbl_led_v_hl_1.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_v_hl_1.setToolTipDuration(-5)
        self.lbl_led_v_hl_1.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_v_hl_1.setText("")
        self.lbl_led_v_hl_1.setObjectName("lbl_led_v_hl_1")
        self.lbl_text_d_hl_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_d_hl_1.setGeometry(QtCore.QRect(300, 330, 55, 21))
        self.lbl_text_d_hl_1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_d_hl_1.setFont(font)
        self.lbl_text_d_hl_1.setObjectName("lbl_text_d_hl_1")
        self.lbl_text_d_hr_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_d_hr_0.setGeometry(QtCore.QRect(410, 310, 55, 21))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_d_hr_0.setFont(font)
        self.lbl_text_d_hr_0.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl_text_d_hr_0.setObjectName("lbl_text_d_hr_0")
        self.lbl_led_d_hr_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_d_hr_0.setGeometry(QtCore.QRect(390, 310, 16, 16))
        self.lbl_led_d_hr_0.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_d_hr_0.setToolTipDuration(-5)
        self.lbl_led_d_hr_0.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_d_hr_0.setText("")
        self.lbl_led_d_hr_0.setObjectName("lbl_led_d_hr_0")
        self.lbl_text_d_hl_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_d_hl_0.setGeometry(QtCore.QRect(300, 310, 55, 21))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_d_hl_0.setFont(font)
        self.lbl_text_d_hl_0.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl_text_d_hl_0.setObjectName("lbl_text_d_hl_0")
        self.lbl_text_d_hr_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_d_hr_1.setGeometry(QtCore.QRect(410, 330, 55, 21))
        self.lbl_text_d_hr_1.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_d_hr_1.setFont(font)
        self.lbl_text_d_hr_1.setObjectName("lbl_text_d_hr_1")
        self.lbl_led_d_hl_0 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_d_hl_0.setGeometry(QtCore.QRect(280, 310, 16, 16))
        self.lbl_led_d_hl_0.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_d_hl_0.setToolTipDuration(-5)
        self.lbl_led_d_hl_0.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_d_hl_0.setText("")
        self.lbl_led_d_hl_0.setObjectName("lbl_led_d_hl_0")
        self.lbl_led_d_hr_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_d_hr_1.setGeometry(QtCore.QRect(390, 330, 16, 16))
        self.lbl_led_d_hr_1.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_d_hr_1.setToolTipDuration(-5)
        self.lbl_led_d_hr_1.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_d_hr_1.setText("")
        self.lbl_led_d_hr_1.setObjectName("lbl_led_d_hr_1")
        self.lbl_led_d_hl_1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_d_hl_1.setGeometry(QtCore.QRect(280, 330, 16, 16))
        self.lbl_led_d_hl_1.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_d_hl_1.setToolTipDuration(-5)
        self.lbl_led_d_hl_1.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_d_hl_1.setText("")
        self.lbl_led_d_hl_1.setObjectName("lbl_led_d_hl_1")
        self.lbl_text_input = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_input.setGeometry(QtCore.QRect(730, 10, 64, 23))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.lbl_text_input.setFont(font)
        self.lbl_text_input.setFrameShape(QtWidgets.QFrame.Panel)
        self.lbl_text_input.setFrameShadow(QtWidgets.QFrame.Plain)
        self.lbl_text_input.setLineWidth(2)
        self.lbl_text_input.setMidLineWidth(0)
        self.lbl_text_input.setObjectName("lbl_text_input")
        self.lbl_text_output = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_output.setGeometry(QtCore.QRect(40, 470, 76, 23))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.lbl_text_output.setFont(font)
        self.lbl_text_output.setFrameShape(QtWidgets.QFrame.Panel)
        self.lbl_text_output.setLineWidth(2)
        self.lbl_text_output.setObjectName("lbl_text_output")
        self.lbl_led_out_kl15_sig = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_out_kl15_sig.setGeometry(QtCore.QRect(110, 500, 21, 21))
        self.lbl_led_out_kl15_sig.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_out_kl15_sig.setToolTipDuration(-5)
        self.lbl_led_out_kl15_sig.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_out_kl15_sig.setText("")
        self.lbl_led_out_kl15_sig.setObjectName("lbl_led_out_kl15_sig")
        self.lbl_text_out_kl15_sig = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_out_kl15_sig.setGeometry(QtCore.QRect(140, 500, 55, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_out_kl15_sig.setFont(font)
        self.lbl_text_out_kl15_sig.setObjectName("lbl_text_out_kl15_sig")
        self.lbl_text_out_kl15_rel = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_out_kl15_rel.setGeometry(QtCore.QRect(270, 500, 55, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_out_kl15_rel.setFont(font)
        self.lbl_text_out_kl15_rel.setObjectName("lbl_text_out_kl15_rel")
        self.lbl_led_out_kl15_rel = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_out_kl15_rel.setGeometry(QtCore.QRect(240, 500, 21, 21))
        self.lbl_led_out_kl15_rel.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_out_kl15_rel.setToolTipDuration(-5)
        self.lbl_led_out_kl15_rel.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_out_kl15_rel.setText("")
        self.lbl_led_out_kl15_rel.setObjectName("lbl_led_out_kl15_rel")
        self.lbl_text_out_br_l = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_out_br_l.setGeometry(QtCore.QRect(400, 500, 55, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_out_br_l.setFont(font)
        self.lbl_text_out_br_l.setObjectName("lbl_text_out_br_l")
        self.lbl_led_out_br_l = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_out_br_l.setGeometry(QtCore.QRect(370, 500, 21, 21))
        self.lbl_led_out_br_l.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_out_br_l.setToolTipDuration(-5)
        self.lbl_led_out_br_l.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_out_br_l.setText("")
        self.lbl_led_out_br_l.setObjectName("lbl_led_out_br_l")
        self.lbl_text_out_br_r = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_out_br_r.setGeometry(QtCore.QRect(530, 500, 55, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_out_br_r.setFont(font)
        self.lbl_text_out_br_r.setObjectName("lbl_text_out_br_r")
        self.lbl_led_out_br_r = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_out_br_r.setGeometry(QtCore.QRect(500, 500, 21, 21))
        self.lbl_led_out_br_r.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_out_br_r.setToolTipDuration(-5)
        self.lbl_led_out_br_r.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_out_br_r.setText("")
        self.lbl_led_out_br_r.setObjectName("lbl_led_out_br_r")
        self.lbl_text_out_sl_hl = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_out_sl_hl.setGeometry(QtCore.QRect(200, 580, 55, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_out_sl_hl.setFont(font)
        self.lbl_text_out_sl_hl.setObjectName("lbl_text_out_sl_hl")
        self.lbl_led_out_sl_hl = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_out_sl_hl.setGeometry(QtCore.QRect(170, 580, 21, 21))
        self.lbl_led_out_sl_hl.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_out_sl_hl.setToolTipDuration(-5)
        self.lbl_led_out_sl_hl.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_out_sl_hl.setText("")
        self.lbl_led_out_sl_hl.setObjectName("lbl_led_out_sl_hl")
        self.lbl_text_out_nsl_r = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_out_nsl_r.setGeometry(QtCore.QRect(330, 580, 55, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_out_nsl_r.setFont(font)
        self.lbl_text_out_nsl_r.setObjectName("lbl_text_out_nsl_r")
        self.lbl_led_out_nsl_r = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_out_nsl_r.setGeometry(QtCore.QRect(300, 580, 21, 21))
        self.lbl_led_out_nsl_r.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_out_nsl_r.setToolTipDuration(-5)
        self.lbl_led_out_nsl_r.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_out_nsl_r.setText("")
        self.lbl_led_out_nsl_r.setObjectName("lbl_led_out_nsl_r")
        self.lbl_text_out_nsl_l = QtWidgets.QLabel(self.centralwidget)
        self.lbl_text_out_nsl_l.setGeometry(QtCore.QRect(460, 580, 55, 20))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.lbl_text_out_nsl_l.setFont(font)
        self.lbl_text_out_nsl_l.setObjectName("lbl_text_out_nsl_l")
        self.lbl_led_out_nsl_l = QtWidgets.QLabel(self.centralwidget)
        self.lbl_led_out_nsl_l.setGeometry(QtCore.QRect(430, 580, 21, 21))
        self.lbl_led_out_nsl_l.setMaximumSize(QtCore.QSize(16777215, 16777214))
        self.lbl_led_out_nsl_l.setToolTipDuration(-5)
        self.lbl_led_out_nsl_l.setStyleSheet("QLabel {\n"
"    background-color: white;\n"
"    border-radius: 8px;\n"
"    border: 2px solid gray;\n"
"}\n"
"")
        self.lbl_led_out_nsl_l.setText("")
        self.lbl_led_out_nsl_l.setObjectName("lbl_led_out_nsl_l")
        self.lbl_input = QtWidgets.QLabel(self.centralwidget)
        self.lbl_input.setGeometry(QtCore.QRect(730, 10, 381, 461))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.lbl_input.setFont(font)
        self.lbl_input.setFrameShape(QtWidgets.QFrame.Box)
        self.lbl_input.setLineWidth(2)
        self.lbl_input.setText("")
        self.lbl_input.setObjectName("lbl_input")
        self.lbl_output = QtWidgets.QLabel(self.centralwidget)
        self.lbl_output.setGeometry(QtCore.QRect(40, 470, 691, 181))
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        self.lbl_output.setFont(font)
        self.lbl_output.setFrameShape(QtWidgets.QFrame.Box)
        self.lbl_output.setLineWidth(2)
        self.lbl_output.setText("")
        self.lbl_output.setObjectName("lbl_output")
        self.lbl_output.raise_()
        self.lbl_input.raise_()
        self.car_pic.raise_()
        self.lbl_send_zat1.raise_()
        self.lbl_send_zat2.raise_()
        self.btn_send_zat1.raise_()
        self.btn_send_zat2.raise_()
        self.lbl_text_zat1.raise_()
        self.lbl_text_zat2.raise_()
        self.lbl_led_zat1.raise_()
        self.lbl_led_zat2.raise_()
        self.btn_Details.raise_()
        self.btn_Refresh.raise_()
        self.lbl_send_v_hr_0.raise_()
        self.lbl_send_v_hr_1.raise_()
        self.btn_send_v_hr_0.raise_()
        self.btn_send_v_hr_1.raise_()
        self.lbl_send_v_hl_0.raise_()
        self.lbl_send_v_hl_1.raise_()
        self.btn_send_v_hl_0.raise_()
        self.btn_send_v_hl_1.raise_()
        self.lbl_send_d_hr_0.raise_()
        self.btn_send_d_hr_0.raise_()
        self.lbl_send_d_hr_1.raise_()
        self.btn_send_d_hr_1.raise_()
        self.lbl_send_d_hl_0.raise_()
        self.lbl_send_d_hl_1.raise_()
        self.btn_send_d_hl_0.raise_()
        self.btn_send_d_hl_1.raise_()
        self.lbl_send_soft_0.raise_()
        self.lbl_send_soft_1.raise_()
        self.btn_send_soft_0.raise_()
        self.btn_send_soft_1.raise_()
        self.lbl_send_mot1.raise_()
        self.btn_send_mot1.raise_()
        self.lbl_led_mot1.raise_()
        self.lbl_text_mot1.raise_()
        self.lbl_led_soft_0.raise_()
        self.lbl_led_soft_1.raise_()
        self.lbl_text_soft_0.raise_()
        self.lbl_text_soft_1.raise_()
        self.lbl_led_v_hr_0.raise_()
        self.lbl_led_v_hr_1.raise_()
        self.lbl_text_v_hr_0.raise_()
        self.lbl_text_v_hr_1.raise_()
        self.lbl_text_v_hl_0.raise_()
        self.lbl_led_v_hl_0.raise_()
        self.lbl_text_v_hl_1.raise_()
        self.lbl_led_v_hl_1.raise_()
        self.lbl_text_d_hl_1.raise_()
        self.lbl_text_d_hr_0.raise_()
        self.lbl_led_d_hr_0.raise_()
        self.lbl_text_d_hl_0.raise_()
        self.lbl_text_d_hr_1.raise_()
        self.lbl_led_d_hl_0.raise_()
        self.lbl_led_d_hr_1.raise_()
        self.lbl_led_d_hl_1.raise_()
        self.lbl_text_input.raise_()
        self.lbl_text_output.raise_()
        self.lbl_led_out_kl15_sig.raise_()
        self.lbl_text_out_kl15_sig.raise_()
        self.lbl_text_out_kl15_rel.raise_()
        self.lbl_led_out_kl15_rel.raise_()
        self.lbl_text_out_br_l.raise_()
        self.lbl_led_out_br_l.raise_()
        self.lbl_text_out_br_r.raise_()
        self.lbl_led_out_br_r.raise_()
        self.lbl_text_out_sl_hl.raise_()
        self.lbl_led_out_sl_hl.raise_()
        self.lbl_text_out_nsl_r.raise_()
        self.lbl_led_out_nsl_r.raise_()
        self.lbl_text_out_nsl_l.raise_()
        self.lbl_led_out_nsl_l.raise_()
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1139, 23))
        self.menubar.setObjectName("menubar")
        self.menuVWMEBBCU19SAM = QtWidgets.QMenu(self.menubar)
        self.menuVWMEBBCU19SAM.setObjectName("menuVWMEBBCU19SAM")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menubar.addAction(self.menuVWMEBBCU19SAM.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.lbl_send_zat1.setText(_translate("MainWindow", "kl15_zat1"))
        self.lbl_send_zat2.setText(_translate("MainWindow", "kl15_zat2"))
        self.btn_send_zat1.setText(_translate("MainWindow", "Trimite semnal"))
        self.btn_send_zat2.setText(_translate("MainWindow", "Trimite semnal"))
        self.lbl_text_zat1.setText(_translate("MainWindow", "kl15_zat1"))
        self.lbl_text_zat2.setText(_translate("MainWindow", "kl15_zat2"))
        self.btn_Details.setText(_translate("MainWindow", "Detalii"))
        self.btn_Refresh.setText(_translate("MainWindow", "Refresh"))
        self.lbl_send_v_hr_0.setText(_translate("MainWindow", "v_hr_0"))
        self.lbl_send_v_hr_1.setText(_translate("MainWindow", "v_hr_1"))
        self.btn_send_v_hr_0.setText(_translate("MainWindow", "Trimite semnal"))
        self.btn_send_v_hr_1.setText(_translate("MainWindow", "Trimite semnal"))
        self.lbl_send_v_hl_0.setText(_translate("MainWindow", "v_hl_0"))
        self.lbl_send_v_hl_1.setText(_translate("MainWindow", "v_hl_1"))
        self.btn_send_v_hl_0.setText(_translate("MainWindow", "Trimite semnal"))
        self.btn_send_v_hl_1.setText(_translate("MainWindow", "Trimite semnal"))
        self.lbl_send_d_hr_0.setText(_translate("MainWindow", "d_hr_0"))
        self.btn_send_d_hr_0.setText(_translate("MainWindow", "Trimite semnal"))
        self.lbl_send_d_hr_1.setText(_translate("MainWindow", "d_hr_1"))
        self.btn_send_d_hr_1.setText(_translate("MainWindow", "Trimite semnal"))
        self.lbl_send_d_hl_0.setText(_translate("MainWindow", "d_hl_0"))
        self.lbl_send_d_hl_1.setText(_translate("MainWindow", "d_hl_1"))
        self.btn_send_d_hl_0.setText(_translate("MainWindow", "Trimite semnal"))
        self.btn_send_d_hl_1.setText(_translate("MainWindow", "Trimite semnal"))
        self.lbl_send_soft_0.setText(_translate("MainWindow", "softtouch_0"))
        self.lbl_send_soft_1.setText(_translate("MainWindow", "softtouch_1"))
        self.btn_send_soft_0.setText(_translate("MainWindow", "Trimite semnal"))
        self.btn_send_soft_1.setText(_translate("MainWindow", "Trimite semnal"))
        self.lbl_send_mot1.setText(_translate("MainWindow", "mot_haube1"))
        self.btn_send_mot1.setText(_translate("MainWindow", "Trimite semnal"))
        self.lbl_text_mot1.setText(_translate("MainWindow", "mot_haube1"))
        self.lbl_text_soft_0.setText(_translate("MainWindow", "softtouch_0"))
        self.lbl_text_soft_1.setText(_translate("MainWindow", "softtouch_1"))
        self.lbl_text_v_hr_0.setText(_translate("MainWindow", "v_hr_0"))
        self.lbl_text_v_hr_1.setText(_translate("MainWindow", "v_hr_1"))
        self.lbl_text_v_hl_0.setText(_translate("MainWindow", "v_hl_0"))
        self.lbl_text_v_hl_1.setText(_translate("MainWindow", "v_hl_1"))
        self.lbl_text_d_hl_1.setText(_translate("MainWindow", "d_hl_1"))
        self.lbl_text_d_hr_0.setText(_translate("MainWindow", "d_hr_0"))
        self.lbl_text_d_hl_0.setText(_translate("MainWindow", "d_hl_0"))
        self.lbl_text_d_hr_1.setText(_translate("MainWindow", "d_hr_1"))
        self.lbl_text_input.setText(_translate("MainWindow", "Input:"))
        self.lbl_text_output.setText(_translate("MainWindow", "Output:"))
        self.lbl_text_out_kl15_sig.setText(_translate("MainWindow", "kl15_sig"))
        self.lbl_text_out_kl15_rel.setText(_translate("MainWindow", "kl15_rel"))
        self.lbl_text_out_br_l.setText(_translate("MainWindow", "br_l"))
        self.lbl_text_out_br_r.setText(_translate("MainWindow", "br_r"))
        self.lbl_text_out_sl_hl.setText(_translate("MainWindow", "sl_hl"))
        self.lbl_text_out_nsl_r.setText(_translate("MainWindow", "nsl_r"))
        self.lbl_text_out_nsl_l.setText(_translate("MainWindow", "nsl_l"))
        self.menuVWMEBBCU19SAM.setTitle(_translate("MainWindow", "VWMEB19BCUSAM"))


    #functie pentru a doua fereastra
    def openWindow(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_SecondWindow()
        self.ui.setupUi(self.window)
        self.window.show()


    def refresh(self):
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()
        # Citeste semnalele din xml pe rand si le updateaza   
        kl15_sig_element = root.find("./AnalogOutput_DUT[@Name='kl15_sig']")
        value_element = kl15_sig_element.find("values")

        if value_element is not None:
            current_value = int(value_element.text)
    
            if current_value == 1:
                self.lbl_led_out_kl15_sig.setStyleSheet("QLabel {\n"
                                                        "    background-color: darkgoldenrod;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
            else:
                self.lbl_led_out_kl15_sig.setStyleSheet("QLabel {\n"
                                                        "    background-color: white;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
        else:
            self.lbl_led_out_kl15_sig.setStyleSheet("QLabel {\n"
                                                    "    background-color: white;\n"
                                                    "    border-radius: 8px;\n"
                                                    "    border: 2px solid gray;\n"
                                                    "}\n"
                                                    "")





        kl15_rel_element = root.find("./AnalogOutput_DUT[@Name='kl15_rel']")
        value_element = kl15_rel_element.find("values")

        if value_element is not None:
            current_value = int(value_element.text)
    
            if current_value == 1:
                self.lbl_led_out_kl15_rel.setStyleSheet("QLabel {\n"
                                                        "    background-color: darkgoldenrod;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
            else:
                self.lbl_led_out_kl15_rel.setStyleSheet("QLabel {\n"
                                                        "    background-color: white;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
        else:
            self.lbl_led_out_kl15_rel.setStyleSheet("QLabel {\n"
                                                    "    background-color: white;\n"
                                                    "    border-radius: 8px;\n"
                                                    "    border: 2px solid gray;\n"
                                                    "}\n"
                                                    "")




        br_l_element = root.find("./AnalogOutput_DUT[@Name='br_l']")
        value_element = br_l_element.find("values")

        if value_element is not None:
            current_value = int(value_element.text)
    
            if current_value == 1:
                self.lbl_led_out_br_l.setStyleSheet("QLabel {\n"
                                                        "    background-color: darkgoldenrod;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
            else:
                self.lbl_led_out_br_l.setStyleSheet("QLabel {\n"
                                                        "    background-color: white;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
        else:
            self.lbl_led_out_br_l.setStyleSheet("QLabel {\n"
                                                    "    background-color: white;\n"
                                                    "    border-radius: 8px;\n"
                                                    "    border: 2px solid gray;\n"
                                                    "}\n"
                                                    "")


        br_r_element = root.find("./AnalogOutput_DUT[@Name='br_r']")
        value_element = br_r_element.find("values")

        if value_element is not None:
            current_value = int(value_element.text)
    
            if current_value == 1:
                self.lbl_led_out_br_r.setStyleSheet("QLabel {\n"
                                                        "    background-color: darkgoldenrod;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
            else:
                self.lbl_led_out_br_r.setStyleSheet("QLabel {\n"
                                                        "    background-color: white;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
        else:
            self.lbl_led_out_br_r.setStyleSheet("QLabel {\n"
                                                    "    background-color: white;\n"
                                                    "    border-radius: 8px;\n"
                                                    "    border: 2px solid gray;\n"
                                                    "}\n"
                                                    "")




        sl_hl_element = root.find("./AnalogOutput_DUT[@Name='sl_hl']")
        value_element = sl_hl_element.find("values")

        if value_element is not None:
            current_value = int(value_element.text)
    
            if current_value == 1:
                self.lbl_led_out_sl_hl.setStyleSheet("QLabel {\n"
                                                        "    background-color: darkgoldenrod;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
            else:
                self.lbl_led_out_sl_hl.setStyleSheet("QLabel {\n"
                                                        "    background-color: white;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
        else:
            self.lbl_led_out_sl_hl.setStyleSheet("QLabel {\n"
                                                    "    background-color: white;\n"
                                                    "    border-radius: 8px;\n"
                                                    "    border: 2px solid gray;\n"
                                                    "}\n"
                                                    "")



        nsl_r_element = root.find("./AnalogOutput_DUT[@Name='nsl_r']")
        value_element = nsl_r_element.find("values")

        if value_element is not None:
            current_value = int(value_element.text)
    
            if current_value == 1:
                self.lbl_led_out_nsl_r.setStyleSheet("QLabel {\n"
                                                        "    background-color: darkgoldenrod;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
            else:
                self.lbl_led_out_nsl_r.setStyleSheet("QLabel {\n"
                                                        "    background-color: white;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
        else:
            self.lbl_led_out_nsl_r.setStyleSheet("QLabel {\n"
                                                    "    background-color: white;\n"
                                                    "    border-radius: 8px;\n"
                                                    "    border: 2px solid gray;\n"
                                                    "}\n"
                                                    "")




        nsl_l_element = root.find("./AnalogOutput_DUT[@Name='nsl_l']")
        value_element = nsl_l_element.find("values")

        if value_element is not None:
            current_value = int(value_element.text)
    
            if current_value == 1:
                self.lbl_led_out_nsl_l.setStyleSheet("QLabel {\n"
                                                        "    background-color: darkgoldenrod;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
            else:
                self.lbl_led_out_nsl_l.setStyleSheet("QLabel {\n"
                                                        "    background-color: white;\n"
                                                        "    border-radius: 8px;\n"
                                                        "    border: 2px solid gray;\n"
                                                        "}\n"
                                                        "")
        else:
            self.lbl_led_out_nsl_l.setStyleSheet("QLabel {\n"
                                                    "    background-color: white;\n"
                                                    "    border-radius: 8px;\n"
                                                    "    border: 2px solid gray;\n"
                                                    "}\n"
                                                    "")





    def clicked_zat1(self):         
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()

        # Gaseste elementul in functie de butonul apasat
        kl15_zat1_element = root.find("./DigitalInput_DUT[@Name='kl15_zat1']")
        # Cauta in fisierul xml campul cu valori a semnalului
        values = kl15_zat1_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_zat1_on:
            self.lbl_led_zat1.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_zat1_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_zat1.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_zat1.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_zat1.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_zat1_on = True

    def clicked_zat2(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()

        # Gaseste elementul in functie de butonul apasat
        kl15_zat2_element = root.find("./DigitalInput_DUT[@Name='kl15_zat2']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = kl15_zat2_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_zat2_on:
            self.lbl_led_zat2.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_zat2_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_zat2.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_zat2.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_zat2.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_zat2_on = True

    def clicked_vhr_0(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()


        # Gaseste elementul in functie de butonul apasat
        v_hr_0_element = root.find("./DigitalInput_DUT[@Name='v_hr_0']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = v_hr_0_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_v_hr_0_on:
            self.lbl_led_v_hr_0.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_v_hr_0_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_v_hr_0.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_v_hr_0.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_v_hr_0.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_v_hr_0_on = True

    def clicked_vhr_1(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()

        # Gaseste elementul in functie de butonul apasat
        v_hr_1_element = root.find("./DigitalInput_DUT[@Name='v_hr_1']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = v_hr_1_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_v_hr_1_on:
            self.lbl_led_v_hr_1.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_v_hr_1_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_v_hr_1.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_v_hr_1.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_v_hr_1.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_v_hr_1_on = True

    def clicked_vhl_0(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()


        # Gaseste elementul in functie de butonul apasat
        v_hl_0_element = root.find("./DigitalInput_DUT[@Name='v_hl_0']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = v_hl_0_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_v_hl_0_on:
            self.lbl_led_v_hl_0.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_v_hl_0_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_v_hl_0.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_v_hl_0.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_v_hl_0.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_v_hl_0_on = True

    def clicked_vhl_1(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()

        
        # Gaseste elementul in functie de butonul apasat
        v_hl_1_element = root.find("./DigitalInput_DUT[@Name='v_hl_1']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = v_hl_1_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_v_hl_1_on:
            self.lbl_led_v_hl_1.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_v_hl_1_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_v_hl_1.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_v_hl_1.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_v_hl_1.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_v_hl_1_on = True

    def clicked_dhr_0(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()


        # Gaseste elementul in functie de butonul apasat
        d_hr_0_element = root.find("./DigitalInput_DUT[@Name='d_hr_0']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = d_hr_0_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_d_hr_0_on:
            self.lbl_led_d_hr_0.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_d_hr_0_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_d_hr_0.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_d_hr_0.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_d_hr_0.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_d_hr_0_on = True

    def clicked_dhr_1(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()

        # Gaseste elementul in functie de butonul apasat
        d_hr_1_element = root.find("./DigitalInput_DUT[@Name='d_hr_1']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = d_hr_1_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_d_hr_1_on:
            self.lbl_led_d_hr_1.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_d_hr_1_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_d_hr_1.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_d_hr_1.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_d_hr_1.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_d_hr_1_on = True

    def clicked_dhl_0(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()
        # Executarea fisierului script


        # Gaseste elementul in functie de butonul apasat
        d_hl_0_element = root.find("./DigitalInput_DUT[@Name='d_hl_0']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = d_hl_0_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_d_hl_0_on:
            self.lbl_led_d_hl_0.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_d_hl_0_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_vd_hl_0.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_d_hl_0.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_d_hl_0.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_d_hl_0_on = True

    def clicked_dhl_1(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()


        # Gaseste elementul in functie de butonul apasat
        d_hl_1_element = root.find("./DigitalInput_DUT[@Name='d_hl_1']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = d_hl_1_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_d_hl_1_on:
            self.lbl_led_d_hl_1.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_d_hl_1_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_vd_hl_1.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_d_hl_1.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_d_hl_1.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_d_hl_1_on = True

    def clicked_soft_0(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()


        # Gaseste elementul in functie de butonul apasat
        soft_0_element = root.find("./DigitalInput_DUT[@Name='softtouch_0']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = soft_0_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_soft_0_on:
            self.lbl_led_soft_0.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_soft_0_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_soft_0.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_soft_0.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_soft_0.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_soft_0_on = True

    def clicked_soft_1(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()

        # Gaseste elementul in functie de butonul apasat
        soft_1_element = root.find("./DigitalInput_DUT[@Name='softtouch_1']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = soft_1_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_soft_1_on:
            self.lbl_led_soft_1.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_soft_1_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_soft_1.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_soft_1.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_soft_1.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_soft_1_on = True

    def clicked_mot1(self):
        # Calea catre fisierul script
        bat_file_path = "C:/FAC/Final/run_tester.bat"
        # Executarea fisierului script
        subprocess.call(bat_file_path, shell=True)
        # Citeste fisierul xml
        tree = ET.parse("C:/FAC/Final/ICAS_SAM_HWIO.xml")
        root = tree.getroot()


        # Gaseste elementul in functie de butonul apasat
        mot_1_element = root.find("./DigitalInput_DUT[@Name='mot_haube1_m']")

        # Cauta in fisierul xml campul cu valori a semnalului
        values = mot_1_element.find("values").text.split(',')

        # Incarca starea ledului in functie de valori
        if self.is_led_mot1_on:
            self.lbl_led_mot1.setStyleSheet("QLabel {\n"
                                             "    background-color: darkgoldenrod;\n"
                                             "    border-radius: 8px;\n"
                                             "    border: 2px solid gray;\n"
                                             "}\n"
                                             "")
            self.is_led_mot1_on = False
        else:
            if values:
                # Ia valoarea din xml si il converteste in integer
                current_value = int(values[0])

                if current_value == 1:
                    self.lbl_led_mot1.setStyleSheet("QLabel {\n"
                                                     "    background-color: darkgoldenrod;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
                else:
                    self.lbl_led_mot1.setStyleSheet("QLabel {\n"
                                                     "    background-color: white;\n"
                                                     "    border-radius: 8px;\n"
                                                     "    border: 2px solid gray;\n"
                                                     "}\n"
                                                     "")
            else:
                # Daca nu mai sunt valori date
                self.lbl_led_mot1.setStyleSheet("QLabel {\n"
                                                 "    background-color: white;\n"
                                                 "    border-radius: 8px;\n"
                                                 "    border: 2px solid gray;\n"
                                                 "}\n"
                                                 "")
            self.is_led_mot1_on = True



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
