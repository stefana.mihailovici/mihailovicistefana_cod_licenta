from PyQt5 import QtCore, QtGui, QtWidgets
import tkinter as tk

class Ui_SecondWindow(object):
    def setupUi(self, SecondWindow):
        SecondWindow.setObjectName("SecondWindow")
        SecondWindow.resize(327, 273)
        self.centralwidget = QtWidgets.QWidget(SecondWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.comboBox_signals = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_signals.setGeometry(QtCore.QRect(10, 80, 121, 22))
        self.comboBox_signals.setObjectName("comboBox_signals")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.comboBox_signals.addItem("")
        self.label_text = QtWidgets.QLabel(self.centralwidget)
        self.label_text.setGeometry(QtCore.QRect(170, 80, 121, 100))
        self.label_text.setText("")
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(9)
        self.label_text.setFont(font)
        self.label_text.setObjectName("label_text")
        SecondWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(SecondWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 327, 26))
        self.menubar.setObjectName("menubar")
        SecondWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(SecondWindow)
        self.statusbar.setObjectName("statusbar")
        SecondWindow.setStatusBar(self.statusbar)

        self.retranslateUi(SecondWindow)
        QtCore.QMetaObject.connectSlotsByName(SecondWindow)

    def retranslateUi(self, SecondWindow):
        _translate = QtCore.QCoreApplication.translate
        SecondWindow.setWindowTitle(_translate("SecondWindow", "MainWindow"))
        self.comboBox_signals.setItemText(0, _translate("SecondWindow", "kl15_zat1"))
        self.comboBox_signals.setItemText(1, _translate("SecondWindow", "kl15_zat2"))
        self.comboBox_signals.setItemText(2, _translate("SecondWindow", "v_hr_0"))
        self.comboBox_signals.setItemText(3, _translate("SecondWindow", "v_hr_1"))
        self.comboBox_signals.setItemText(4, _translate("SecondWindow", "v_hl_0"))
        self.comboBox_signals.setItemText(5, _translate("SecondWindow", "v_hl_1"))
        self.comboBox_signals.setItemText(6, _translate("SecondWindow", "d_hr_0"))
        self.comboBox_signals.setItemText(7, _translate("SecondWindow", "d_hr_1"))
        self.comboBox_signals.setItemText(8, _translate("SecondWindow", "d_hl_0"))
        self.comboBox_signals.setItemText(9, _translate("SecondWindow", "d_hl_1"))
        self.comboBox_signals.setItemText(10, _translate("SecondWindow", "softtouch_0"))
        self.comboBox_signals.setItemText(11, _translate("SecondWindow", "softtouch_1"))
        self.comboBox_signals.setItemText(12, _translate("SecondWindow", "mot_haube1_m"))

        self.comboBox_signals.activated.connect(self.clicker)


    #definim functia pentru selectare de semnal
    def clicker(self, index):
        #Stergem ce era inainte
        self.label_text.clear()
        if index == 0:
            text = "Circuit a semnalului de pornire sau oprire a motorului.\nNumele semnalului complet este AIX_KL15_ZAT1_MAIN"
        elif index == 1:
            text = "Circuit a semnalului de pornire sau oprire a motorului.\nNumele semnalului complet este AIX_KL15_ZAT2_MAIN"
        elif index == 2:
            text = "Circuit a modulului de control a usii din dreapta spate.\nNumele semnalului complet este AIX_V_HR."
        elif index == 3:
            text = "Circuit a modulului de control a usii din dreapta spate.\nNumele semnalului complet este AIX_V_HR."
        elif index == 4:
            text = "Circuit a modulului de control a usii din stanga spate.\nNumele semnalului complet este AIX_V_HL."
        elif index == 5:
            text = "Circuit a modulului de control a usii din stanga spate.\nNumele semnalului complet este AIX_V_HL."
        elif index == 6:
            text = "Circuit a modulului de control a usii din dreapta fata.\nNumele semnalului complet este AIX_D_HR."
        elif index == 7:
            text = "Circuit a modulului de control a usii din dreapta fata.\nNumele semnalului complet este AIX_D_HR."
        elif index == 8:
            text = "Circuit a modulului de control a usii din stanga fata.\nNumele semnalului complet este AIX_D_HL."
        elif index == 9:
            text = "Circuit a modulului de control a usii din stanga fata.\nNumele semnalului complet este AIX_D_HL."
        elif index == 10:
            text = "Circuit a semnalului de la manerul electric exterior al usii din spate.\nNumele semnalului complet este AIX_S_SOFTTOUCH."
        elif index == 11:
            text = "Circuit a semnalului de la manerul electric exterior al usii din spate.\nNumele semnalului complet este AIX_S_SOFTTOUCH."
        elif index == 12:
            text = "Circuit a semnalului de la contactul de sub capota.\nNumele semnalului complet este AIX_MOT_HAUBE1_M."

        self.label_text.setText(text)

        # Ajusteaza lungimea labelului in functie de textul dat
        self.label_text.adjustSize()




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    SecondWindow = QtWidgets.QMainWindow()
    ui = Ui_SecondWindow()
    ui.setupUi(SecondWindow)
    SecondWindow.show()
    sys.exit(app.exec_())
